fun main(args: Array<String>) {

    var list = mutableListOf(10,100,20,50,30)
    var list2 = mutableListOf("withdraw 2 10", "transfer 5 1 20", "deposit 5 20", "transfer 3 4 15")
//    var list2 = mutableListOf<String>()

    fun bankRequests(accounts: MutableList<Int>, requests: MutableList<String>): MutableList<Int> {
        var adjustedAccounts = accounts
        var error = mutableListOf<Int>()

        fun deposit(account: Int, amount: Int) {
            adjustedAccounts[account] += amount
        }

        for ((index, request) in requests.withIndex()) {
            var currentRequest = request.split(' ')
            println("request=$request")
            println("currentRequest=$currentRequest")
            var currentAccount = currentRequest[1].toInt() - 1
            println("currentAccount=$currentAccount")

            if (currentAccount < 0 || currentAccount > adjustedAccounts.size - 1) {
                error.add((index + 1) * -1)
                return error
            }

            var currentBalance = adjustedAccounts[currentAccount]
            var amount = currentRequest[2].toInt()

            when (currentRequest[0]) {
                "deposit" -> {
                    deposit(currentAccount, amount)
                }
                "withdraw" -> {
                    if (currentBalance >= amount) {
                        adjustedAccounts[currentAccount] = currentBalance - amount
                    } else {
                        error.add((index + 1) * -1)
                        return error
                    }
                }
                "transfer" -> {
                    var receivingAccount = currentRequest[2].toInt() - 1

                    if (adjustedAccounts.size - 1 < receivingAccount) {
                        error.add((index + 1) * -1)
                        return error
                    }

                    var receivingBalance = adjustedAccounts[receivingAccount]

                    amount = currentRequest[3].toInt()

                    if (currentBalance >= amount) {
                        adjustedAccounts[currentAccount] = currentBalance - amount
                        adjustedAccounts[receivingAccount] = receivingBalance + amount
                    } else {
                        error.add((index + 1) * -1)
                        return error
                    }
                }
                else -> {
                    error.add((index + 1) * -1)
                    return error
                }
            }
        }

        return adjustedAccounts
    }

    println("input=$list, $list2")
    println(bankRequests(list,list2))

}