fun main(args: Array<String>) {

    var s1 = "100[codesignal]"

    fun test(s: String): String {
        var str = s
        val numOfSegments = str.count{ it == ']'}

        for ( segment in 1..numOfSegments) {
            val regexResult = "(\\d+)\\[(\\w+)\\]".toRegex().find(str)
            val code = regexResult!!.value
            val subString = regexResult.groups[2]!!.value
            var decodedString = ""
            val multiplier = regexResult.groups[1]!!.value.toInt()

            for (i in 1..multiplier) {
                decodedString += subString
            }

            str = str.replace(code, decodedString)

        }

        return str
    }

    println("input=$s1")
    println(test(s1))

}