class Playground {

}

fun main(args: Array<String>) {

    var list = mutableListOf(
            mutableListOf(152, 213, 276),
            mutableListOf(274, 259, 151),
            mutableListOf(40, 57, 130),
            mutableListOf(203, 87, 189),
            mutableListOf(43, 182, 163)
    )
    var s1 = "I always wondered why I came to this world."
    var s2 = "For there was never yet philosopher that could endure the toothache patiently. -- William Shakespeare, Much Ado About Nothing"

    fun testFunction(friends: MutableList<MutableList<Int>>, numberOfPasses: Int, startingPlayer: Int): Int {
//        ********** START CODE **********

        class Player(val x: Int, val y: Int, val throwDistance: Int, var catches: Int, val id: Int) {
            var currentDistance = 0.0
        }

        var players = mutableMapOf<Int, Player>()
        for (friend in friends.withIndex()) {
            val player = Player(friend.value[0], friend.value[1], friend.value[2], 0, friend.index)
            players[friend.index] = player
        }

        var catchers = mutableListOf<Int>()
//        var possibleThrows = mutableListOf<Int>()
        var possibleThrows = mutableListOf<Player>()
        var currentPlayer = players[startingPlayer]
        currentPlayer!!.catches++


//        println(players[0]!!.id)
//        println(currentPlayer!!.id)


        while (catchers.size < numberOfPasses) {

            players.forEach {
                val player = it.value
                val x = 1.0 * (player.x - currentPlayer!!.x)
                val y = 1.0 * (player.y - currentPlayer!!.y)
                val distance = Math.sqrt( (x * x) + (y * y) )


                if (distance < currentPlayer!!.throwDistance && player.id != currentPlayer!!.id) {
                    player.currentDistance = distance
                    possibleThrows.add(player)
                    println("distanceCalc=$distance, id=${player.id}")
                }

            }

            var playerWithLeastCatches = possibleThrows[0]
            for (player in possibleThrows) {
                println("id=${player.id}")
                println("dist=${player.currentDistance}")
                println("catches=${player.catches}")


                if (player.catches < playerWithLeastCatches.catches) {
                    playerWithLeastCatches = player
                } else if (player.catches == playerWithLeastCatches.catches) {
                    println("catches equals")
                    if (player.currentDistance > playerWithLeastCatches.currentDistance) playerWithLeastCatches = player
                    println("result is (least) = ${playerWithLeastCatches.id}")
                }
            }
            println("(least)id=${playerWithLeastCatches.id}")
            println("(least)dist=${playerWithLeastCatches.currentDistance}")
            println("(least)catches=${playerWithLeastCatches.catches}")

            currentPlayer = playerWithLeastCatches
            currentPlayer!!.catches++
            catchers.add(currentPlayer.id)

//        break
        }

        println(catchers)
        return catchers.last()
//        ********** END CODE **********
    }

    println("input=$list")
    println(testFunction(list, 19, 4))

    class Person(val x: Int, val y: Int, val distance: Int, val id: Int)
    val b = Person(list[0][0], list[0][1], list[0][2], 1)
    val a = object {
        val x = list[0][0]
        val y = list[0][1]
        val distance = list[0][2]
        val id = 1
    }
//    println(b)
//    println(b.x)
//    println(b.id)


}
